# E-commerce Node.js

Ecommerce Plataform built on Node.js

### .env
    # You will need to create a '.env' file

    Schema:

    PORT= 'A_Port_Of_Your_Choice'
    DB_URL= 'Your_Mongo_DB_URI'

### Notes

    1. For the Build Script: "build": "tsc -p ." to work 
        
        1.1 You will need to change the following package.json lines:
        
        1.2 "_moduleAliases": {
                "@config": "src/config",            <src> => <dist>
                "@controllers": "src/controllers"   <src> => <dist>
            }

            "_moduleAliases": {
                "@config": "dist/config",
                "@controllers": "dist/controllers"
            }
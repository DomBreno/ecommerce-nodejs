import express from 'express'
import mongoose from 'mongoose'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import cors from 'cors'

import routes from './routes'
import config from '@config'

class App {
    public express: express.Application

    public constructor() {
        this.express = express()
        this.middlewares()
        this.db()
        this.routes()
    }

    public boot(): express.Application {
        console.log(`Server On: http://localhost:${config.app.port}`)
        return this.express
    }

    private middlewares(): void {
        this.express.use(cors())
        this.express.use(morgan('dev'))
        this.express.use(bodyParser.json())
        this.express.use(cookieParser())
    }

    private db(): void {
        mongoose.set('useNewUrlParser', true)
        mongoose.set('useCreateIndex', true)
        mongoose.set('useFindAndModify', false)
        mongoose
            .connect(config.app.db.url)
            .then((): void => {
                console.log('Database Connection: Success!')
            })
            .catch((error): void => {
                console.log('Database Connection: Failed!')
                console.log(error.message)
            })
    }

    private routes(): void {
        this.express.use('/api', routes)
    }
}

export default App
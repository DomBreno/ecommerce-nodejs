import { config } from 'dotenv'
config()

export default {
    app: {
        port: process.env.PORT || 8080,
        db: {
            url: process.env.DB_URL || ""
        }
    }
}
import { Router, Request, Response } from 'express'

import ctrl from '@modules/Users/User.controller'

const router = Router()

router.post('/signup', ctrl.signup)

export default router
import { Request, Response } from 'express'

import { UserInterface } from '@modules/Users/User.interface'
import User from '@modules/Users/User.schema'

const ctrl = {
    signup: (req: Request, res: Response) => {
        console.log(`req.body: ${req.body}`)
        const user: UserInterface = new User(req.body)
        user.save((error, user) => {
            if (error) {
                return res.status(400).json({
                    error,
                })
            }
            res.status(200).json({
                user,
            })
        })
    }
}

export default ctrl
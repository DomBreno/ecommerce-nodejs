import { Schema, model, Model } from 'mongoose'
import crypto from 'crypto'
import uuid from 'uuid'
import { UserInterface } from './User.interface';

const userSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: true,
        maxlength: 32
    },

    email: {
        type: String,
        trim: true,
        required: true,
        unique: 32
    },
    hashed_password: {
        type: String,
        required: true
    },
    about: {
        type: String,
        trim: true
    },
    salt: String,
    role: {
        type: Number,
        default: 0
    },
    history: {
        type: Array,
        default: []
    }
}, { timestamps: true })

userSchema.virtual('password')
    .set(function (this: any, password: string) {
        this._password = password
        this.salt = uuid()
        this.hashed_password = this.encryptPassword(password)
    })
    .get(function (this: any) {
        return this._password
    })

userSchema.methods = {
    encryptPassword: function (password: string) {
        if (!password) return ''
        try {
            return crypto.createHmac('sha1', this.salt)
                .update(password)
                .digest('hex')
        } catch (error) {
            return ''
        }
    }
}

export default model<UserInterface>('User', userSchema)
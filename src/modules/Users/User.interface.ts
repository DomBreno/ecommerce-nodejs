import { Document } from 'mongoose'

export interface UserInterface extends Document {
    name: string,
    email: string,
    hashed_password: string,
    about: string,
    salt: string,
    history: Array<any>
}